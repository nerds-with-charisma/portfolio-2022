import { AppConfig } from '@/utils/AppConfig';

const Authors = () => (
  <section id="Authors--cmpt" className="mb-10">
    <a
      href={AppConfig.footer.authors[0]?.url}
      className="transition-all text-nwc-purple hover:text-nwc-pink block"
      target="blank"
    >
      {AppConfig.footer.authors[0]?.title}
    </a>
    {' + '}
    <a
      href={AppConfig.footer.authors[1]?.url}
      className="transition-all text-nwc-purple hover:text-nwc-pink block"
      target="blank"
    >
      {AppConfig.footer.authors[1]?.title}
    </a>
  </section>
);

export default Authors;

import { AppConfig } from '@/utils/AppConfig';
import HeadingSM from '../Common/HeadingSM';
import HeadingXS from '../Common/HeadingXS';
import SocialIcons from '../Common/SocialIcons';
import Authors from './Authors';
import Copyright from './Copyright';
import Pi from './Pi';

const Footer = () => (
  <footer
    id="Footer--cmpt"
    className="py-20 text-center leading-loose relative"
  >
    <HeadingSM copy={AppConfig.footer.heading} />
    <Authors />
    <SocialIcons />
    <Copyright />
    <HeadingXS
      copy={AppConfig.footer.tagline}
      theme="text-xs"
    />
    <Pi />
  </footer>
);

export default Footer;

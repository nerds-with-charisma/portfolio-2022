const Pi = () => (
  <button
    type="button"
    id="Pi--cmpt"
    className="absolute bottom-5 right-5 text-gray-500"
    onClick={() =>
      console.log(`
        __.-._
        '-._"7'
        /'.-c
        |  /T
       _)_/LI
       Poke around you can.
       Contact us you will.
    `)
    }
  >
    {'π'}
  </button>
);

export default Pi;

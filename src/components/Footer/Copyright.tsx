import { AppConfig } from '@/utils/AppConfig';

const Copyright = () => (
  <section
    id="Copyright--cmpt"
    className="text-xs"
  >
    &copy; Copyright {new Date().getFullYear()}{' '}
    {AppConfig.title}
  </section>
);

export default Copyright;

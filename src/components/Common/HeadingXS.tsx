import { HeadingSmallTypes } from '@/types/types';

const HeadingXS = ({
  copy,
  theme,
}: HeadingSmallTypes) => (
  <strong className={theme}>{copy}</strong>
);

export default HeadingXS;

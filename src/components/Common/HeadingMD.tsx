import { HeadingSmallTypes } from '@/types/types';

const HeadingMD = ({
  color = 'black',
  copy,
  theme,
}: HeadingSmallTypes) => (
  <h1 className={`text-3xl ${color} ${theme}`}>
    {copy}
  </h1>
);

export default HeadingMD;

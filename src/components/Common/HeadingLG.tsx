import { HeadingSmallTypes } from '@/types/types';

const HeadingLG = ({
  color = 'black',
  copy,
  theme,
}: HeadingSmallTypes) => (
  <h1 className={`text-6xl ${color} ${theme}`}>
    {copy}
  </h1>
);

export default HeadingLG;

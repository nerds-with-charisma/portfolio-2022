import { HeadingSmallTypes } from '@/types/types';

const HeadingSM = ({
  color = 'black',
  copy,
  theme,
}: HeadingSmallTypes) => (
  <strong className={`${color} ${theme}`}>
    {copy}
  </strong>
);

export default HeadingSM;

import { AppConfig } from '@/utils/AppConfig';

const SocialIcons = ({
  color = 'text-nwc-grey',
  stacked = false,
}) => (
  <section id="SocialIcons--cmpt">
    {AppConfig.socialIcons.map((social) => (
      <a
        href={social.url}
        key={social.title}
        className={`transition-all ${color} text-2xl px-4 hover:text-nwc-yellow ${
          stacked === true ? 'block' : ''
        }`}
      >
        <i className={social.icon} />

        <span className="sr-only">
          {social.title}
        </span>
      </a>
    ))}
  </section>
);

export default SocialIcons;

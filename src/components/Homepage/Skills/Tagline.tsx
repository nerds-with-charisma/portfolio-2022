import HeadingSM from '@/components/Common/HeadingSM';
import { AppConfig } from '@/utils/AppConfig';

const Tagline = () => (
  <section
    id="HelpTagline--cmpt"
    className="flex-1 md:hidden lg:hidden rotate-90 text-left relative"
  >
    <HeadingSM
      copy={AppConfig.homepage.skillsTagline}
      color="text-white text-7xl text-shadow-md absolute mt-10"
    />
  </section>
);

export default Tagline;

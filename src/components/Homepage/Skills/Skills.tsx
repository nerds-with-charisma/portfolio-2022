import HeadingSM from '@/components/Common/HeadingSM';
import { AppConfig } from '@/utils/AppConfig';

const Skills = () => (
  <section
    id="Skills--cmpt"
    className="w-full mx-auto align"
  >
    <div className="max-w-2xl mx-auto">
      {AppConfig.homepage.ourSkills.map(
        (skills) => (
          <section
            key={skills.title}
            className="text-center mb-10 md:w-1/2 md:inline-block md:text-xl lg:w-1/2 lg:inline-block lg:text-xl"
          >
            <HeadingSM copy={skills.title} />
            <br />
            {skills.skills.map((skill, i) => (
              <section key={`skill--${i}`}>
                {skill}
              </section>
            ))}
          </section>
        ),
      )}
    </div>
  </section>
);

export default Skills;

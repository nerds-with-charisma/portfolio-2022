import Skills from './Skills';
import Tagline from './Tagline';

import Image from 'next/image';
import SkillsLogos from '../../../assets/images/logo--skills.jpg';

const OurSkills = () => (
  <section
    id="OurSkills--cmpt"
    className="py-20 flex flex-wrap items-center justify-evenly"
  >
    <Tagline />
    <Skills />
    <section className="flex-1 px-20 mix-blend-multiply lg:flex-wrap">
      <div className="md:w-1/3 md:mx-auto">
        <Image
          src={SkillsLogos}
          alt="We do React, GraphQl, Node, Designing with Adobe products, SEO help, develop Wordpress sites, and can teach you how to work in a professional environment if need be."
        />
      </div>
    </section>
  </section>
);

export default OurSkills;

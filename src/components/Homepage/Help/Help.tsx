import HeadingMD from '@/components/Common/HeadingMD';
import { AppConfig } from '@/utils/AppConfig';

const Help = () => (
  <section id="Help--cmpt" className="px-3 py-20">
    <HeadingMD
      copy={AppConfig.homepage.helpHeading}
      theme="text-center"
      color="text-white"
    />
    <div
      className="mt-10 leading-loose text-white text-center mx-auto max-w-3xl"
      dangerouslySetInnerHTML={{
        __html: AppConfig.homepage.helpContent,
      }}
    />
  </section>
);

export default Help;

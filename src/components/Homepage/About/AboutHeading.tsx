import HeadingMD from '@/components/Common/HeadingMD';
import { AppConfig } from '@/utils/AppConfig';

const AboutHeading = () => (
  <section
    id="AboutHeading--cmpt"
    className="mt-10"
  >
    <HeadingMD
      copy={AppConfig.homepage.aboutHeading}
      color="text-nwc-pink"
      theme="font-bold"
    />
  </section>
);

export default AboutHeading;

import { AppConfig } from '@/utils/AppConfig';

const AboutWhats = () => (
  <section
    id="AboutWhats--cmpt"
    className="font-light text-6xl leading-tight"
  >
    {AppConfig.homepage.aboutWhats}
  </section>
);

export default AboutWhats;

import AboutHeading from './AboutHeading';
import AboutImage from './AboutImage';
import AboutWhats from './AboutWhats';

import { motion } from 'framer-motion';

const About = () => (
  <section
    id="About--cmpt"
    className="flex px-3 pt-28  items-center group"
  >
    <div className="flex-1 text-right z-10 relative">
      <motion.div
        initial={false}
        animate={{
          x: ['-400px', '0px'],
          opacity: ['0', '1'],
        }}
        transition={{
          type: 'spring',
          duration: 0.5,
        }}
      >
        <AboutHeading />
        <AboutWhats />
      </motion.div>
    </div>

    <div className="transition-all flex-1  absolute md:relative -left-16 md:-left-0 w-[200px] md:w-auto group-hover:-left-14 z-0">
      <motion.div
        initial={false}
        animate={{
          x: ['900px', '0px'],
          opacity: ['0', '1'],
        }}
        transition={{
          type: 'easeOut',
          duration: 0.5,
        }}
      >
        <AboutImage />
      </motion.div>
    </div>
  </section>
);

export default About;

import { AppConfig } from '@/utils/AppConfig';
import Image from 'next/image';
import PhoneImage from '../../../assets/images/about--phone.png';
const AboutImage = () => (
  <section id="AboutImage--cmpt" className="mx-1">
    <Image
      src={PhoneImage}
      alt={AppConfig.homepage.aboutAlt}
      width={400}
      height={780}
    />
  </section>
);

export default AboutImage;

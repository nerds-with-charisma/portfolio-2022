import HeadingLG from '@/components/Common/HeadingLG';
import SocialIcons from '@/components/Common/SocialIcons';
import { AppConfig } from '@/utils/AppConfig';
import Tagline from './Tagline';

import { motion } from 'framer-motion';

const Hero = () => (
  <section
    id="Hero--cmpt"
    className="relative px-3 pb-28"
  >
    <div className="mx-auto max-w-screen-md">
      <section className="pt-28 leading-tight">
        <motion.div
          initial={false}
          animate={{
            x: ['-400px', '200px', '0px'],
          }}
          transition={{
            type: 'spring',
            duration: 0.8,
          }}
        >
          <HeadingLG
            copy={AppConfig.site_name}
            color="text-white"
            theme="block text-shadow-md font-black md:text-7xl"
          />
        </motion.div>
      </section>
      <Tagline />
      <span className="absolute bottom-3 right-3">
        <SocialIcons color="text-white" stacked />
      </span>
    </div>
  </section>
);

export default Hero;

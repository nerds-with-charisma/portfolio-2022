import { TaglineTypes } from '@/types/types';
import { AppConfig } from '@/utils/AppConfig';
import { useEffect, useState } from 'react';

const Tagline = ({ copy }: TaglineTypes) => {
  const [rq, rqSetter] = useState(
    AppConfig.homepage.heroTagline[0],
  );

  useEffect(() => {
    rqSetter(
      AppConfig.homepage.heroTagline[
        Math.floor(
          Math.random() *
            AppConfig.homepage.heroTagline.length,
        )
      ],
    );
  }, []);

  return (
    <section id="Tagline--cmpt">
      <h2 className="font-light text-white text-4xl pt-3 pl-1 text-shadow-md">
        {copy || rq}
      </h2>
    </section>
  );
};

export default Tagline;

import HeadingSM from '@/components/Common/HeadingSM';
import { PortfolioPageType } from '@/types/types';

const Technology = ({
  page,
}: PortfolioPageType) => (
  <section
    id="Technology--cmpt"
    className="leading-loose my-10 text-lg"
  >
    <HeadingSM copy="Technology" />
    <br />
    {page.workObj.stack}
  </section>
);

export default Technology;

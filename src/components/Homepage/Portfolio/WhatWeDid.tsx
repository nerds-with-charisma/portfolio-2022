import HeadingSM from '@/components/Common/HeadingSM';

const WhatWeDid = () => (
  <section id="WhatWeDid--cmpt">
    <HeadingSM copy="What We Did" />
  </section>
);

export default WhatWeDid;

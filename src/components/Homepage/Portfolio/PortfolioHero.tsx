import HeadingLG from '@/components/Common/HeadingLG';
import Tagline from '@/components/Homepage/Hero/Tagline';
import { PortfolioPageType } from '@/types/types';
import Link from 'next/link';

const PortfolioHero = ({
  page,
}: PortfolioPageType) => (
  <section
    id="PortfolioHero--cmpt"
    className="relative px-3 pb-28"
    style={{
      background: `url(${page.workObj.hero.src}) no-repeat center top fixed`,
    }}
  >
    <div className="mx-auto max-w-screen-md">
      <section className="pt-28 leading-tight">
        <HeadingLG
          copy={page.workObj.title}
          color="text-white"
          theme="block text-shadow-md font-black md:text-7xl"
        />
      </section>
      <Tagline
        copy={`${page.workObj.tech
          .toString()
          .replace(/,/g, ',  ')} :: ${
          page.workObj.launched
        }`}
      />

      {page.workObj.externalUrl && (
        <Link href={page.workObj.externalUrl}>
          <a>Launch</a>
        </Link>
      )}
    </div>
  </section>
);

export default PortfolioHero;

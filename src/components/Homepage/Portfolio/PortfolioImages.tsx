import { PortfolioPageType } from '@/types/types';
import Image from 'next/image';

const imageClasses =
  'rounded-xl drop-shadow-lg overflow-hidden inline-block flex-1 text-center';

const PortfolioImages = ({
  page,
}: PortfolioPageType) => (
  <section
    id="PortfolioImages--cmpt"
    className="flex"
  >
    {page.workObj.image1 && (
      <div className={imageClasses}>
        <Image
          alt={`${page.workObj.title} work overview`}
          src={page?.workObj?.image1!}
          width={214}
          height={1439}
          className="rounded-xl"
        />
      </div>
    )}
    {page.workObj.image2 && (
      <div className={imageClasses}>
        <br />
        <br />
        <br />
        <Image
          alt={`${page.workObj.title} detail shot`}
          src={page?.workObj?.image2!}
          width={214}
          height={603}
          className="rounded-xl"
        />

        <br />
        <br />
        <br />

        <Image
          alt={`${page.workObj.title} interior example`}
          src={page?.workObj?.image3!}
          width={214}
          height={603}
          className="rounded-xl"
        />
      </div>
    )}
  </section>
);

export default PortfolioImages;

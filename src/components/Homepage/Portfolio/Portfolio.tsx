import HeadingMD from '@/components/Common/HeadingMD';
import { AppConfig } from '@/utils/AppConfig';
import Image from 'next/image';
import Link from 'next/link';

import { motion } from 'framer-motion';

const Portfolio = () => (
  <section id="Portfolio--cmpt" className="px-3">
    <HeadingMD
      copy={AppConfig.homepage.portfolioHeading}
      color="text-black text-center mt-20 mb-10"
    />

    <section className="text-center max-w-5xl mx-auto">
      {AppConfig.homepage.portfolioItems.map(
        (item) => (
          <section
            key={item.workObj.title}
            className="w-1/2 md:w-1/3 lg:w-1/3 inline-block mb-4 px-1"
          >
            <Link href={`/works/${item.slug}`}>
              <a className="transition-all opaicty-100 drop-shadow-md hover:opacity-80 hover:drop-shadow-2xl">
                <motion.div
                  initial={false}
                  animate={{
                    opacity: ['0', '1'],
                  }}
                  transition={{
                    type: 'easeOut',
                    duration: 0.8,
                  }}
                >
                  <Image
                    src={item?.thumb!}
                    width={228}
                    height={130}
                    alt={item.workObj.title}
                    className="bg-white rounded-md"
                  />
                </motion.div>
              </a>
            </Link>
          </section>
        ),
      )}
    </section>

    <HeadingMD
      copy={
        AppConfig.homepage.portfolioSubHeading
      }
      color="text-white text-shadow-md text-center mt-10 mb-10"
    />

    <section className="text-center  max-w-5xl mx-auto">
      {AppConfig.homepage.portfolioOtherLogos.map(
        (logo) => (
          <section
            key={logo.title}
            className="w-1/3 md:w-1/4 px-1 inline-block mb-8"
          >
            <span className="drop-shadow-sm">
              <motion.div
                initial={false}
                animate={{
                  opacity: ['0', '1.0'],
                }}
                transition={{
                  type: 'easeOut',
                  duration: 0.8,
                }}
              >
                <Image
                  src={logo.src}
                  width={228}
                  height={130}
                  alt={logo.title}
                  className="bg-white"
                />
              </motion.div>
            </span>
          </section>
        ),
      )}
    </section>
  </section>
);

export default Portfolio;

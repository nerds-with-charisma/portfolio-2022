import HeadingSM from '@/components/Common/HeadingSM';
import { PortfolioPageType } from '@/types/types';

const Details = ({ page }: PortfolioPageType) => (
  <section
    id="Details--cmpt"
    className="leading-loose mt-20 mb-20 text-lg"
  >
    <HeadingSM copy="Details &amp; Goals" />
    <br />
    {page.workObj.description}
  </section>
);

export default Details;

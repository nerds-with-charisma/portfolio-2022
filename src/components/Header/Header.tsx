import Script from 'next/script';
import Logo from './Logo';

const Header = () => (
  <header
    id="Header--cmpt"
    className="px-3 fixed top-3 z-10"
  >
    <Script
      src="https://kit.fontawesome.com/c565b86e8f.js"
      crossOrigin="anonymous"
    ></Script>
    <Logo />
  </header>
);

export default Header;

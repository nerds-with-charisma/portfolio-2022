import { AppConfig } from '@/utils/AppConfig';
import Image from 'next/image';
import Link from 'next/link';

const Logo = () => (
  <section id="Logo--cmpt" className="pt-6">
    <Link href="/">
      <a>
        <div className="w-10 transition-all opacity-100 hover:opacity-90 drop-shadow-none hover:drop-shadow-md">
          <Image
            src={AppConfig.header.logoSrc}
            alt={AppConfig.header.logoAlt}
            width={AppConfig.header.logoWidth}
            height={AppConfig.header.logoHeight}
          />
        </div>
      </a>
    </Link>
  </section>
);

export default Logo;

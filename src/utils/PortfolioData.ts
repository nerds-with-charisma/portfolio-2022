import heroDFB from '../assets/images/portfolio/bg--daily-fit-blend.jpg';
import DailyFitBlend1 from '../assets/images/portfolio/daily-fit-blend-1.jpg';
import DailyFitBlend2 from '../assets/images/portfolio/daily-fit-blend-2.jpg';
import DailyFitBlend3 from '../assets/images/portfolio/daily-fit-blend-3.jpg';
import thumbDailyFitBlend from '../assets/images/thumb--daily-fit-blend.webp';

import heroUpendo from '../assets/images/portfolio/bg--upendo.jpg';
import Upendo1 from '../assets/images/portfolio/upendo-1.jpg';
import Upendo2 from '../assets/images/portfolio/upendo-2.jpg';
import Upendo3 from '../assets/images/portfolio/upendo-3.jpg';
import thumbUpendo from '../assets/images/thumb--upendo.webp';

import heroSears from '../assets/images/portfolio/bg--sears-outlet.jpg';
import Sears1 from '../assets/images/portfolio/sears-1.jpg';
import Sears2 from '../assets/images/portfolio/sears-2.jpg';
import Sears3 from '../assets/images/portfolio/sears-3.jpg';
import thumbSears from '../assets/images/thumb--af.webp';

import heroNerdFit from '../assets/images/portfolio/bg--nerd-fit.jpg';
import NerdFit1 from '../assets/images/portfolio/nerd-fit-1.jpg';
import NerdFit2 from '../assets/images/portfolio/nerd-fit-2.jpg';
import NerdFit3 from '../assets/images/portfolio/nerd-fit-3.jpg';
import thumbNerdFit from '../assets/images/thumb--nerd-fit.webp';

import heroChamberlain from '../assets/images/portfolio/bg--chamberlain.jpg';
import Chamberlain1 from '../assets/images/portfolio/chamberlain-1.jpg';
import Chamberlain2 from '../assets/images/portfolio/chamberlain-2.jpg';
import Chamberlain3 from '../assets/images/portfolio/chamberlain-3.jpg';
import thumbChamberlain from '../assets/images/thumb--chamberlain.webp';

// import Axiom1 from '../assets/images/portfolio/axiom-1.jpg';
// import Axiom2 from '../assets/images/portfolio/axiom-2.jpg';
// import Axiom3 from '../assets/images/portfolio/axiom-3.jpg';
import heroAxiom from '../assets/images/portfolio/bg--axiom.jpg';
import thumbAxiom from '../assets/images/thumb--axiom.webp';

export const portfolioData = [
  {
    created_at: '2021-05-03T17:18:06.128Z',
    id: 2,
    published_at: '2021-05-03T17:18:37.993Z',
    slug: 'daily-fit-blend',
    thumb: thumbDailyFitBlend,
    updated_at: '2021-05-03T18:13:49.454Z',
    workObj: {
      description:
        'This was a fun project. Daily Fit Blend is a startup company that approached us to design and build their website meant to collect sign-ups and provide info about their product. The project was built using the MERN stack (MongoDB, Express, React, and NodeJs) and deployed to Heroku. We also provided SEO support and marketing strategy to set the company up for continued growth.',
      domain: 'nerdswithcharisma.com',
      hero: heroDFB,
      image:
        'https://nerdswithcharisma.com/nwc-tile.jpg',
      image1: DailyFitBlend1,
      image2: DailyFitBlend2,
      image3: DailyFitBlend3,
      launched: '2021',
      meta: [],
      og: null,
      protocol: 'https:',
      seo: null,
      site_name: 'Nerds With Charisma',
      stack:
        '[ NodeJs, React, MongoDb, HTML5/CSS3, Google Analytics, Marketing, SEO, Design, Web Development ]',
      tech: ['Design', 'Marketing & Website'],
      title: 'Daily Fit Blend',
      type: 'website',
      url: 'https://clean-power.us/',
    },
  },
  {
    created_at: '2021-04-20T17:45:46.628Z',
    id: 1,
    published_at: '2021-04-20T17:56:37.765Z',
    slug: 'upendo-staffing',
    thumb: thumbUpendo,
    updated_at: '2021-05-03T18:07:34.371Z',

    workObj: {
      description:
        'Upendo Staffing is a recruiting firm based in Florida that previously had a simple, static Wix website. We gave them a jolt & hooked them up with a SUPER-sweet Jamstack site that allows them full control of their content as well as a job board to attract potential recruits and improve their workflow!',
      domain: 'nerdswithcharisma.com',
      hero: heroUpendo,
      image:
        'https://nerdswithcharisma.com/nwc-tile.jpg',
      image1: Upendo1,
      image2: Upendo2,
      image3: Upendo3,
      launched: '2020',
      meta: [],
      og: null,
      protocol: 'https:',
      seo: null,
      site_name: 'Nerds With Charisma',
      stack: '[Gatsby, React, GraphQl, Adobe Xd]',
      tech: [
        'Web Dev',
        'Jamstack',
        'Design & UX',
      ],
      title: 'Upendo Staffing',
      type: 'website',
      url: 'https://upendostaffing.com/',
    },
  },
  {
    created_at: '2021-05-04T17:35:21.553Z',
    id: 4,
    published_at: '2021-05-04T17:35:22.719Z',
    slug: 'sears-outlet',
    thumb: thumbSears,
    updated_at: '2021-05-04T17:46:33.308Z',
    workObj: {
      description:
        "American Freight / Sears Hometown & Outlet has been my full-time home for the past 5+ years. In the time there I have worked on so many projects and had the privilege of leading the development lifecycle, using the latest and greatest cutting edge technologies. Nowhere else that I've worked has been so encouraging of using and adopting new technologies. Some of the projects involved at SHO include, the bread and butter website, SearsOutlet, the more rural websites Sears Hometown, Sears Hardware...",
      domain: 'nerdswithcharisma.com',
      hero: heroSears,
      image:
        'https://nerdswithcharisma.com/nwc-tile.jpg',
      image1: Sears1,
      image2: Sears2,
      image3: Sears3,
      launched: '2014',
      meta: [],
      og: null,
      protocol: 'https:',
      seo: null,
      site_name: 'Nerds With Charisma',
      stack:
        '[ ReactJs, Analytics, Marketing, SEO, Branding, Design, Wordpress, NetSuite / Suite Commerce Advanced, BackboneJs ]',
      tech: [
        'Web Development',
        'Marketing',
        'eCommerce',
      ],
      title: 'American Freight / Sears Outlet',
      type: 'website',
      url: 'https://americanfreight.com/',
    },
  },
  {
    created_at: '2021-05-04T17:24:47.904Z',
    id: 3,
    published_at: '2021-05-04T17:25:12.352Z',
    slug: 'nerd-fit',
    thumb: thumbNerdFit,
    updated_at: '2021-05-04T17:34:25.183Z',
    workObj: {
      description:
        "Nerd Fit is a lifestyle app meant to keep your life in order. It tracks your calories burned, steps, workouts, todos, grocery list, and more. This was an amazing project to build, and probably the most fun we've had on a project in a long time. NF will be in the appstore shortly.",
      domain: 'nerdswithcharisma.com',
      hero: heroNerdFit,
      image:
        'https://nerdswithcharisma.com/nwc-tile.jpg',
      image1: NerdFit1,
      image2: NerdFit2,
      image3: NerdFit3,
      launched: '2022',
      meta: [],
      og: null,
      protocol: 'https:',
      seo: null,
      site_name: 'Nerds With Charisma',
      stack:
        '[ Mobile Development, Social Networking]',
      tech: ['Flutter', 'Firebase'],
      title: 'Nerd Fit',
      type: 'website',
      url: 'https://nerdswithcharisma.com/',
    },
  },
  {
    created_at: '2021-05-04T17:47:24.202Z',
    id: 5,
    published_at: '2021-05-04T17:47:25.821Z',
    slug: 'chamberlain-staffing',
    thumb: thumbChamberlain,
    updated_at: '2021-05-04T17:50:40.373Z',
    workObj: {
      description:
        'The great guys at Chamberlain came to us to help get their online presence setup. This included their initial branding and online portal. We worked hand and hand with them to create an experience tailor made for their clients, as well as streamline their existing processes.',
      domain: 'nerdswithcharisma.com',
      hero: heroChamberlain,
      image:
        'https://nerdswithcharisma.com/nwc-tile.jpg',
      image1: Chamberlain1,
      image2: Chamberlain2,
      image3: Chamberlain3,
      launched: '2016',
      meta: [],
      og: null,
      protocol: 'https:',
      seo: null,
      site_name: 'Nerds With Charisma',
      stack:
        '[ Angular, Design, Branding, Marketing, SEO ]',
      tech: ['Web Development & Branding'],
      title: 'Chamberlain Staffing',
      type: 'website',
      url: 'https://www.chamberlainadvisors.co/',
    },
  },
  {
    created_at: '2021-05-05T15:04:21.201Z',
    id: 6,
    published_at: '2021-05-05T15:04:22.551Z',
    slug: 'axiom-technology-group',
    thumb: thumbAxiom,
    updated_at: '2021-05-05T15:11:25.693Z',
    workObj: {
      description:
        'For Axiom we did a complete and total re-brand. Everything from the ground up was reworked, redesigned, and built from scratch. Marketing collateral, business cards, internal documents, resumes, website, internal applications, etc... This was a long project over the course of several years. Everything from the lowly pixel, to giant SEO related tasks were fair game. Axiom was tons of fun to work on, in a true end-to-end fashion.',
      domain: 'nerdswithcharisma.com',
      hero: heroAxiom,
      image:
        'https://nerdswithcharisma.com/nwc-tile.jpg',
      image1: null,
      image2: null,
      image3: null,
      launched: '2016',
      meta: [],
      og: null,
      protocol: 'https:',
      seo: null,
      site_name: 'Nerds With Charisma',
      stack:
        '[ Wordpress, Design, Branding, Marketing, Creative ]',
      tech: [
        'Development',
        'Marketing',
        'SEO',
        'Branding',
        'Kitchen Sink',
      ],
      title: 'Axiom Tech Group',
      type: 'website',
      url: 'https://axiomtechgroup.com/',
    },
  },
];

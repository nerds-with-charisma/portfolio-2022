import { portfolioData } from './PortfolioData';

import Logo from '../assets/images/logo--nwc.png';
import thumbBfg from '../assets/images/thumb--bfg.webp';
import thumbCampPlayAlot from '../assets/images/thumb--camp-play-alot.webp';
import thumbLadse from '../assets/images/thumb--ladse.webp';
import thumbNiu from '../assets/images/thumb--niu-outreach.webp';
import thumbObChamber from '../assets/images/thumb--ob-chamber.webp';
import thumbPremier from '../assets/images/thumb--premier-academy.webp';
import thumbSos from '../assets/images/thumb--sos-illinois.webp';
import thumbStericycle from '../assets/images/thumb--stericycle.webp';

export const AppConfig = {
  site_name: 'Nerds With Charisma',
  title: 'nerds with charisma',
  description:
    'Starter code for your Nextjs Boilerplate with Tailwind CSS',
  locale: 'en',
  header: {
    logoSrc: Logo,
    logoAlt:
      'Nerds With Charisma - Designing and developing amazing apps and websites',
    logoHeight: 57,
    logoWidth: 105,
  },
  footer: {
    heading: 'Designed & Developed By',
    tagline:
      "Where we're going, we don't need roads.",
    authors: [
      {
        title: 'brian dausman',
        url: 'https://docs.google.com/document/d/1kmNAqGPVtvv2EYF4JjCTKOBr7tus0-sQlAJRbu0gswY/edit#heading=h.5x0d5h95i329',
      },
      {
        title: 'andrew bieganski',
        url: 'https://www.andrewbieganski.com/images/andrewBieganski2020.pdf',
      },
    ],
  },
  homepage: {
    aboutHeading: 'What We Do',
    aboutWhats: 'websites. apps. seo. better.',
    aboutImage: 'null',
    aboutAlt:
      'Nerd Fit fitness app for workout tracking and lifestyle modification. Built with Flutter for Android.',
    heroAlt:
      'Website, wordpress, React, development. Company branding, design services, and brand identity & strategy all under one roof.',
    heroTagline: [
      'Coding Since Before We Could Walk.',
      'The Best in the World at What We Do.',
      'Super-Awesome Apps for Super-Awesome Peeps.',
      "I Think We're Gonna Need A Bigger Boat.",
      'Coding Ninjas.',
      'Punk Rock Pixels.',
      'The Best There Is, Was, and Ever Will Be.',
      'Do Or Do Not. There Is No Try.',
      'We Know "The Google".',
      'FOR SCIENCE!',
      'Reinforced by Space-Age Technology.',
    ],
    portfolioHeading: 'Our Most Recent Works',
    portfolioSubHeading:
      "Other Clients We've Helped",
    portfolioItems: portfolioData,
    portfolioOtherLogos: [
      {
        src: thumbCampPlayAlot,
        title: 'Camp Play-A-Lot',
      },
      {
        src: thumbPremier,
        title: 'Premier Acadamy',
      },
      {
        src: thumbLadse,
        title: 'LADSE',
      },
      {
        src: thumbObChamber,
        title: 'OakBrook Chamber',
      },
      {
        src: thumbSos,
        title: 'SOS Illinois',
      },
      {
        src: thumbStericycle,
        title: 'Stericycle',
      },
      {
        src: thumbBfg,
        title: 'BFG Tech',
      },
      {
        src: thumbNiu,
        title: 'NIU Outreach',
      },
    ],
    skillsTagline: 'Skills',
    ourSkills: [
      {
        title: 'Development',
        skills: [
          'NextJs / Gatsby',
          'JamStack',
          'Mobile Apps',
          'Wordpress Websites',
          'SEO & Marketing',
          'Analytics, Tagging, Tracking Pixels',
          'Backups & Monitoring',
          'eCommerce',
        ],
      },
      {
        title: 'Design',
        skills: [
          'Business Cards',
          'Logos & Branding',
          'Stationary',
          'E-Blasts',
          'Social & Strategy',
          'Keyword Research',
          'Analytics',
          'And More...',
        ],
      },
    ],
    helpHeading: 'We Can Help Build Your Brand',
    helpContent: `<div>Nerds With Charisma are a small band of designers, developers, & creatives. Our unique process involves a hands on, back-and-forth approach to get your project off the ground and into the wild exactly as you envision it.
    <br /><br />
    We can bring fresh ideas and a new approach to your brand. We don't just build your website, we build and cultivate your online identity. We collaborate closely with you to learn your business, discover new opportunities, and bring your ideas to life. We’re also very comfortable working with people pretty much anywhere in the world.
    <br /><br />
    We utilize the latest technologies & trends to get your site setup and rocking. Our help doesn’t stop at just a website. Sure, we can help design & develop your site, but we will also discuss and explain your user’s experience, come up with a content strategy for continued success, get you going on social media, explain the new technologies, go over analytics, and get your SEO juice flowing. We also really enjoy doing websites for nonprofits.
    <br /><br />
    We can work with you and your budget to get your projects rolling. Simply shoot us an email below, and we’ll let you know what we can do.</div>`,
  },
  socialIcons: [
    {
      title: 'GitLab',
      url: 'https://gitlab.com/nerds-with-charisma',
      icon: 'fa-brands fa-gitlab',
    },
    {
      title: 'NPM',
      url: 'https://www.npmjs.com/~nerds-with-charisma',
      icon: 'fa-brands fa-react',
    },
    {
      title: 'Twitter',
      url: 'https://twitter.com/nerdswcharisma',
      icon: 'fa-brands fa-twitter',
    },
  ],
  seo: {
    description:
      'Nerds With Charisma is a cutting edge digital media agency specializing in awesome websites.',
    gaId: 'G-12CYVEFHPV',
    gtmId: 'GTM-P5648LD',
    keywords:
      'Nerds with Charisma, Brian Dausman, Andrew Bieganski, Web Design Chicago, Awesome Websites, React Developers, Websites for Non-Profits, Bitchin Websites',
    themeColor: '#663399',
    title:
      'Nerds With Charisma. Awesome Websites, Brian Dausman & Andrew Bieganski',
  },
};

export interface HeadingSmallTypes {
  color?: string;
  copy: string;
  theme?: string;
}

export interface TaglineTypes {
  copy?: string;
}

export interface PortfolioPageType {
  page: PortfolioType;
}

export interface PortfolioType {
  slug: string;
  workObj: WorkTypes;
}

export interface WorkTypes {
  description: string;
  hero: PortfolioHeroType;
  launched?: string;
  image1?: string;
  image2?: string;
  image3?: string;
  tech: [];
  title: string;
  stack?: [];
  externalUrl?: string;
}

interface PortfolioHeroType {
  src: string;
}

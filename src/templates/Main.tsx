import { ReactNode } from 'react';

import Footer from '@/components/Footer/Footer';
import Header from '@/components/Header/Header';

type IMainProps = {
  meta: ReactNode;
  children: ReactNode;
};

const Main = (props: IMainProps) => (
  <div className="w-full antialiased h-screen pt-0">
    {props.meta}

    <div>
      <Header />

      <main id="main">{props.children}</main>

      <Footer />
    </div>
  </div>
);

export { Main };

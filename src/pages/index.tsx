import About from '@/components/Homepage/About/About';
import Help from '@/components/Homepage/Help/Help';
import Hero from '@/components/Homepage/Hero/Hero';
import Portfolio from '@/components/Homepage/Portfolio/Portfolio';
import OurSkills from '@/components/Homepage/Skills/OurSkills';
import { Meta } from '@/layouts/Meta';
import { Main } from '@/templates/Main';
import { AppConfig } from '@/utils/AppConfig';

const Index = () => {
  const {
    description,
    keywords,
    gaId,
    gtmId,
    themeColor,
    title,
  } = AppConfig.seo;
  return (
    <Main
      meta={
        <Meta
          title={title}
          description={description}
          keywords={keywords}
          themeColor={themeColor}
          gtmId={gtmId}
          gaId={gaId}
        />
      }
    >
      <Hero />
      <About />
      <Portfolio />
      <OurSkills />
      <Help />
    </Main>
  );
};

export default Index;

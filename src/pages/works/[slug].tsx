import Details from '@/components/Homepage/Portfolio/Details';
import PortfolioHero from '@/components/Homepage/Portfolio/PortfolioHero';
import PortfolioImages from '@/components/Homepage/Portfolio/PortfolioImages';
import Technology from '@/components/Homepage/Portfolio/Technology';
import { Meta } from '@/layouts/Meta';
import { Main } from '@/templates/Main';
import { PortfolioPageType } from '@/types/types';
import { portfolioData } from '@/utils/PortfolioData';

export default function Page({
  page,
}: PortfolioPageType) {
  return (
    <Main
      meta={
        <Meta
          title="Next.js Boilerplate Presentation"
          description="Next js Boilerplate is the perfect starter code for your project. Build your React application with the Next.js framework."
        />
      }
    >
      <PortfolioHero page={page} />
      <section className="px-5">
        <Details page={page} />
        <PortfolioImages page={page} />
        <Technology page={page} />
        {/* <WhatWeDid /> */}
      </section>
    </Main>
  );
}

export async function getStaticPaths() {
  const paths = portfolioData.map((page: any) => {
    return {
      params: {
        slug: page.slug,
      },
    };
  });

  return { paths, fallback: false };
}

export async function getStaticProps({
  params,
}: any) {
  console.log(params);
  const currentPath = params.slug;
  const page = portfolioData.find(
    (page) => page.slug === currentPath,
  );

  return {
    props: {
      page,
    },
  };
}
